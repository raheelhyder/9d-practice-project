package com.example.podtime.StorageApp.Fragments

import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.podtime.R
import kotlin.math.roundToInt


class RecentAdapter(
    private val recent: ArrayList<recentActivity>,
) :
    RecyclerView.Adapter<RecentAdapter.AllItemViewHolder>() {
    //private var powerMenu: PowerMenu? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllItemViewHolder {
        val allItem = LayoutInflater.from(parent.context).inflate(R.layout.single_recent_activity_item, parent, false)
        return AllItemViewHolder(allItem)
    }

    override fun onBindViewHolder(holder: AllItemViewHolder, position: Int) {
        val recent: recentActivity = recent[position]
        holder.activityName.text = recent.sharedActivity
        holder.activitySize.text = recent.sharedFileSize
        holder.cnLayout.setOnClickListener {
            it.findNavController().navigate(R.id.action_storageFragment_to_detailsFragment)
        }
        holder.dots.setOnClickListener {
            editTextDialogue(it, position) { updatedName ->
                holder.activityName.text = updatedName
                recent.sharedActivity = updatedName
                notifyDataSetChanged()
            }
            /*val originalPos = IntArray(2)
            it.getLocationInWindow(originalPos)
            val x = originalPos[0]
            val y = originalPos[1]
            Log.e("position", "onBindViewHolder: $x $y")
            val onMenuItemClickListener: OnMenuItemClickListener<PowerMenuItem?> =
                OnMenuItemClickListener<PowerMenuItem?> { position, item ->
                    when (position) {
                        0 -> {
                            Toast.makeText(holder.dots.context, "File Deleted", Toast.LENGTH_SHORT)
                                .show()
                        }
                        1 -> {
                            Toast.makeText(
                                holder.dots.context,
                                "File Recovered",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        else -> {
                            Toast.makeText(holder.dots.context, "Properties", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    //Toast.makeText(holder.dots.context, item.title, Toast.LENGTH_SHORT).show()
                    powerMenu?.selectedPosition = position // change selected item
                    powerMenu?.dismiss()
                }

            powerMenu = PowerMenu.Builder(it.context)
                .addItem(PowerMenuItem("Delete", false)) // add an item.
                .addItem(PowerMenuItem("Recover", false)) // aad an item list.
                .addItem(PowerMenuItem("Properties", false)) // aad an item list.
                .setAnimation(MenuAnimation.SHOWUP_TOP_LEFT) // Animation start point (TOP | LEFT).
                .setMenuRadius(30f) // sets the corner radius.
                .setMenuShadow(10f) // sets the shadow.
                .setTextColor(ContextCompat.getColor(it.context, R.color.black))
                .setTextGravity(Gravity.START)
                .setTextSize(15)
                .setHeight(400)
                .setFocusable(true)
                .setCircularEffect(CircularEffect.BODY) // shows circular revealed effects for all body of the popup menu.
                .setTextTypeface(Typeface.create("@font/alata", Typeface.NORMAL))
                .setSelectedTextColor(Color.WHITE)
                .setMenuColor(Color.WHITE)
                .setSelectedMenuColor(ContextCompat.getColor(it.context, R.color.teal_700))
                .setOnMenuItemClickListener(onMenuItemClickListener)
                .build()


            val metrics: DisplayMetrics = it.context.resources.displayMetrics
            val width = metrics.widthPixels
            val height = metrics.heightPixels

            if (height - y > powerMenu?.contentViewHeight!!) {
                powerMenu?.showAsDropDown(it, -(powerMenu!!.contentViewWidth), -(it.height))
                Log.e("height", "onScrolled: $height $y ${height - y > powerMenu?.contentViewHeight!!} ${height - y}")
                Log.e("height", "onScrolled: ${powerMenu?.contentViewHeight!!}")
            } else {
                powerMenu?.showAsDropDown(it, -(powerMenu!!.contentViewWidth), (powerMenu!!.contentViewHeight / 2))
                Log.e("height", "onScrolled: $height $y ${height - y > powerMenu?.contentViewHeight!!} ${height - y}")
            }*/
        }
    }

    override fun getItemCount(): Int {
        return recent.size
    }

    private fun editTextDialogue(
        view: View,
        position: Int,
        onNameUpdated: (newName: String) -> Unit
    ) {
        /*try {
            val window = PopupWindow(view)
            val inflater = view.context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val layout = inflater.inflate(R.layout.popup, null, false)
            window.isFocusable = true
            window.contentView = layout

            window.isOutsideTouchable = true
            window.setOnDismissListener {}
            val values = IntArray(2)
            view.getLocationInWindow(values)
            val positionOfIcon = values[1]
            val displayMetrics = view.context.resources?.displayMetrics
            val height = displayMetrics?.heightPixels!! * 2 / 3
            if (positionOfIcon > height) {
                window.showAsDropDown(view, 0, -320)
            } else {
                window.showAsDropDown(view, 0, 0)
            }
            window.showAsDropDown(view, 100, 0, 1)
            val delete = layout.findViewById(R.id.menu_delete) as ConstraintLayout
            val properties = layout.findViewById(R.id.menu_recover) as ConstraintLayout
            val recover = layout.findViewById(R.id.menu_properties) as ConstraintLayout


            delete.setOnClickListener {
                Toast.makeText(view.context, "File Deleted", Toast.LENGTH_SHORT).show()
                layout.visibility = View.INVISIBLE
            }

            properties.setOnClickListener {
                Toast.makeText(view.context, "Data Recovered", Toast.LENGTH_SHORT).show()
                layout.visibility = View.INVISIBLE
            }

            recover.setOnClickListener {
                Toast.makeText(view.context, "Properties Enabled", Toast.LENGTH_SHORT).show()
                layout.visibility = View.INVISIBLE
            }
        } catch (e: Exception) {
        }*/
        val container = view.findViewById<ConstraintLayout>(R.id.container)
        val popupView: View =
            LayoutInflater.from(view.context).inflate(R.layout.popup_layout, container, false)
        val rename = popupView.findViewById<ConstraintLayout>(R.id.reset)
        val del = popupView.findViewById<ConstraintLayout>(R.id.delete_item)
        val share = popupView.findViewById<ConstraintLayout>(R.id.share_item)
        val popupWindow = PopupWindow(
            popupView,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            changeDpToPx(180F, view.context)
        )
        popupWindow.isOutsideTouchable = true
        popupWindow.setOnDismissListener {}
        val values = IntArray(2)
        view.getLocationInWindow(values)
        val positionOfIcon = values[1]
        val displayMetrics = view.context.resources?.displayMetrics
        val height = displayMetrics?.heightPixels!! * 2 / 3
        if (positionOfIcon > height) {
            popupWindow.showAsDropDown(view, 0, -380)
        } else {
            popupWindow.showAsDropDown(view, 0, 0)
        }
        popupWindow.showAsDropDown(view, 100, 0, 1)

        rename.setOnClickListener {
            updateUser(view.context, recent[position].sharedActivity) { updatedName ->
                onNameUpdated(updatedName)
            }
            popupWindow.dismiss()
        }
        del.setOnClickListener {
            deleteUser(it.context, (position))
            popupWindow.dismiss()
        }
        share.setOnClickListener {
            Toast.makeText(view.context, "File Share", Toast.LENGTH_SHORT).show()
            popupWindow.dismiss()
        }
    }

    private fun deleteUser(context: Context, position: Int) {
        val viewLayout = LayoutInflater.from(context).inflate(R.layout.delete_dialogue, null)
        val deleteDialog: AlertDialog = AlertDialog.Builder(context).create()
        deleteDialog.setView(viewLayout)
        deleteDialog.show()
        deleteDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val doneButton: TextView? = deleteDialog.findViewById(R.id.dialogue_done_btn)
        val exitButton: TextView? = deleteDialog.findViewById(R.id.dialogue_exit_btn)

        doneButton?.setOnClickListener {
            recent.removeAt(position)
            notifyDataSetChanged()
            Log.e("Delete", "deleteUser: $recent")
            deleteDialog.dismiss()
        }
        exitButton?.setOnClickListener {
            deleteDialog.dismiss()
        }
    }

    private fun updateUser(
        context: Context,
        textViewValue: String,
        onNameUpdated: (newName: String) -> Unit
    ) {

        val viewLayout = LayoutInflater.from(context).inflate(R.layout.rename_dialogue, null)
        val updateDialog: AlertDialog = AlertDialog.Builder(context).create()
        updateDialog.setView(viewLayout)
        updateDialog.show()
        updateDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val doneButton: TextView? = updateDialog.findViewById(R.id.dialogue_done_btn)
        val exitButton: TextView? = updateDialog.findViewById(R.id.dialogue_exit_btn)
        val activityNameEditText: EditText? = updateDialog.findViewById(R.id.dialogue_et)

        activityNameEditText?.setText(textViewValue)
        activityNameEditText?.setSelection(0, textViewValue.length)
        doneButton?.setOnClickListener {
            onNameUpdated(activityNameEditText?.text.toString())
            activityNameEditText?.imeOptions = EditorInfo.IME_ACTION_DONE
            val imm: InputMethodManager? =
                context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm?.hideSoftInputFromWindow(it.windowToken, 0)
            updateDialog.dismiss()
        }
        exitButton?.setOnClickListener {
            activityNameEditText?.imeOptions = EditorInfo.IME_ACTION_DONE
            val imm: InputMethodManager? =
                context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm?.hideSoftInputFromWindow(it.windowToken, 0)
            updateDialog.dismiss()
        }
    }

    private fun changeDpToPx(value: Float, context: Context): Int {
        val r: Resources = context.resources
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, value, r.displayMetrics
        ).roundToInt()
    }

/*
    private fun showPopup(view: View) {
        val wrapper: Context = ContextThemeWrapper(view.context, R.style.PopupDefault)
        val popup = PopupMenu(wrapper, view, Gravity.END)
        popup.inflate(R.menu.menu)
        popup.setOnMenuItemClickListener { item: MenuItem? ->
            when (item!!.itemId) {
                R.id.menu_delete -> {
                    Toast.makeText(view.context, "File Deleted", Toast.LENGTH_SHORT).show()
                }
                R.id.menu_recover -> {
                    Toast.makeText(view.context, "Data Recovered", Toast.LENGTH_SHORT).show()
                }
                R.id.menu_properties -> {
                    Toast.makeText(view.context, "Properties Enabled", Toast.LENGTH_SHORT).show()
                }
            }
            true
        }

        popup.show()
    }
*/

    class AllItemViewHolder(Item: View) : RecyclerView.ViewHolder(Item) {
        val activityName: TextView = itemView.findViewById((R.id.tv_file_name))
        val activitySize: TextView = itemView.findViewById((R.id.tv_file_size))
        val cnLayout: ConstraintLayout = itemView.findViewById((R.id.cn_layout_recent))
        val dots: ImageView = itemView.findViewById(R.id.three_dots)
    }
}