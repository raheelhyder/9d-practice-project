package com.example.podtime.StorageApp.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.podtime.databinding.FragmentBundleBinding

class BundleFragment : Fragment() {
    private lateinit var binding: FragmentBundleBinding
    private lateinit var bundleUpgrade: ArrayList<BundleUpgrade>
    private lateinit var bundleType: Array<String>
    private lateinit var bundleSize: Array<String>
    private lateinit var bundlePrice: Array<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentBundleBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bundleType = arrayOf(
            "Premium",
            "Pro",
            "Business",
            "Premium",
            "Pro",
            "Business",
            "Pro",
            "Business",
            "Premium",
            "Premium"
        )

        bundleSize = arrayOf(
            "200 GB",
            "10 GB",
            "12 TB",
            "15 GB",
            "18 MB",
            "2 GB",
            "2000 MB",
            "59 GB",
            "274 TB",
            "25 MB"
        )

        bundlePrice = arrayOf(
            "$ 229 / Month",
            "$ 45 / Month",
            "$ 172 / Month",
            "$ 9 / Month",
            "$ 1200 / Month",
            "$ 63 / Month",
            "$ 211 / Month",
            "$ 633 / Month",
            "$ 243 / Month",
            "$ 132 / Month",
        )

        binding.rvMonthlyBundles.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvMonthlyBundles.setHasFixedSize(true)
        bundleUpgrade = arrayListOf()
        getBundleData()

        binding.bundleBackPressIv.setOnClickListener{
            findNavController().popBackStack()
        }
    }

    private fun getBundleData() {
        for (i in bundleType.indices) {
            val bundle = BundleUpgrade(bundleType[i],bundleSize[i],bundlePrice[i])
            bundleUpgrade.add(bundle)
        }
        binding.rvMonthlyBundles.adapter = BundleAdapter(bundleUpgrade)
    }
}