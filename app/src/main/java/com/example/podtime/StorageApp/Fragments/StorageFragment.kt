package com.example.podtime.StorageApp.Fragments

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.podtime.R
import com.example.podtime.databinding.FragmentStorageBinding


class StorageFragment : Fragment() {
    private lateinit var binding: FragmentStorageBinding
    private lateinit var sharedFileArray: ArrayList<SharedFile>
    private lateinit var recentActivityArray: ArrayList<recentActivity>
    private lateinit var fileName: Array<String>
    private lateinit var activityName: Array<String>
    private lateinit var activitySize: Array<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentStorageBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fileName = arrayOf(
            "Script file",
            "Documents",
            "Gallery",
            "Desktop",
            "Folder1",
            "Access",
            "Flood",
            "Physics",
            "Flower",
            "Drinks"
        )

        activityName = arrayOf(
            "Recovery Code",
            "Voice Recorder",
            "IMG22022983",
            "IMG22052452",
            "IMG22024239",
            "IMG22022242",
            "IMG22035983",
            "IMG22602283",
            "IMG22022343",
            "IMG22022983"
        )

        activitySize = arrayOf(
            "148 MB",
            "11 MB",
            "1 GB",
            "1400 KB",
            "23 MB",
            "842 KB",
            "450 GB",
            "409 KB",
            "480 KB",
            "146 GB"
        )

        binding.rvSharedFile.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.rvSharedFile.setHasFixedSize(true)
        sharedFileArray = arrayListOf()
        getFilesData()

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvRecentActivity.layoutManager = layoutManager
        binding.rvRecentActivity.setHasFixedSize(true)
        recentActivityArray = arrayListOf()
        getRecentActivityData()


        /*Log.e("onScroll", "onScrolled 1: ${layoutManager.findLastVisibleItemPosition()}", )
        binding.rvRecentActivity.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (layoutManager.findLastVisibleItemPosition() == layoutManager.itemCount - 1 && dy > 0) {
                    Log.e("onScroll", "onScrolled 2: ${layoutManager.findLastVisibleItemPosition()}", )
                    //adapter.powerMenu?.showAsDropDown(view, -360, -116)
                } else {
                    Log.e("onScroll", "onScrolled 3: ${layoutManager.findLastVisibleItemPosition()}", )
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })*/
    }

    private fun getFilesData() {
        for (i in fileName.indices) {
            val sharedFile = SharedFile(fileName[i])
            sharedFileArray.add(sharedFile)
        }
        binding.rvSharedFile.adapter = SharedFileAdapter(sharedFileArray)
    }

    private fun getRecentActivityData() {
        for (i in activityName.indices) {
            val recent = recentActivity(activityName[i], activitySize[i])
            recentActivityArray.add(recent)
        }
        binding.rvRecentActivity.adapter = RecentAdapter(recentActivityArray)
    }
}