package com.example.podtime.StorageApp.Fragments

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.podtime.R

class BundleAdapter(
    private val bundle: ArrayList<BundleUpgrade>
) :
    RecyclerView.Adapter<BundleAdapter.AllItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllItemViewHolder {
        val allItem =
            LayoutInflater.from(parent.context).inflate(R.layout.single_bundle_item, parent, false)
        return AllItemViewHolder(allItem)
    }

    override fun onBindViewHolder(holder: AllItemViewHolder, position: Int) {
        val bundle: BundleUpgrade = bundle[position]
        holder.bundleType.text = bundle.bundleType
        holder.bundleSize.text = bundle.bundleSize
        holder.bundlePrice.text = bundle.bundlePrice
    }

    override fun getItemCount(): Int {
        return bundle.size
    }

    class AllItemViewHolder(Item: View) : RecyclerView.ViewHolder(Item) {
        val bundleType: TextView = itemView.findViewById((R.id.tv_bundle_type))
        val bundleSize: TextView = itemView.findViewById((R.id.tv_bundle_size))
        val bundlePrice: TextView = itemView.findViewById((R.id.tv_bundle_price))
    }
}
