package com.example.podtime.StorageApp.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.podtime.R
import com.example.podtime.databinding.FragmentDetailBinding
import com.example.podtime.databinding.FragmentDetailsBinding
import com.example.podtime.databinding.FragmentStorageBinding


class DetailsFragment : Fragment() {
    private lateinit var binding: FragmentDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.backPressIv.setOnClickListener{
            findNavController().popBackStack()
        }

        binding.detailsCnItem.setOnClickListener{
            findNavController().navigate(R.id.action_detailsFragment_to_bundleFragment)
        }

        Glide.with(this)
            .load(R.drawable.download_1)
            .circleCrop()
            .into(binding.recent1Iv)

        Glide.with(this)
            .load(R.drawable.download_2)
            .circleCrop()
            .into(binding.recent2Iv)

        Glide.with(this)
            .load(R.drawable.download_3)
            .circleCrop()
            .into(binding.recent3Iv)
    }
}