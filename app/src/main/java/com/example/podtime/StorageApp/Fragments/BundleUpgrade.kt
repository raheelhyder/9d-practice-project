package com.example.podtime.StorageApp.Fragments

data class BundleUpgrade(
        var bundleType: String = "",
        var bundleSize: String = "",
        var bundlePrice: String = ""
    )
