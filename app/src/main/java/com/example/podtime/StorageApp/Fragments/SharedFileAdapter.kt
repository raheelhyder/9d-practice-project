package com.example.podtime.StorageApp.Fragments

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.podtime.PodtimeApp.Adapter.DataClasses.Topic
import com.example.podtime.R

class SharedFileAdapter(
    private val file: ArrayList<SharedFile>,
) :
    RecyclerView.Adapter<SharedFileAdapter.AllItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllItemViewHolder {
        val allItem = LayoutInflater.from(parent.context).inflate(R.layout.single_shared_item, parent, false)
        return AllItemViewHolder(allItem)
    }

    override fun onBindViewHolder(holder: AllItemViewHolder, position: Int) {
        val file: SharedFile = file[position]
        holder.sharedFile.text = file.sharedFileName

        /*holder.cnLayout.setOnClickListener {
            it.findNavController().navigate(R.id.action_detailFragment_to_detailSubFragment)
            classBack(list[position].watchName, list[position].discountPrice)
        }*/
    }

    override fun getItemCount(): Int {
        return file.size
    }

    class AllItemViewHolder(Item: View) : RecyclerView.ViewHolder(Item) {
        val sharedFile: TextView = itemView.findViewById((R.id.shared_file_tv))
        //val cnLayout: TextView = itemView.findViewById((R.id.cn_sharedFile))
    }
}