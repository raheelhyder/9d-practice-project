package com.example.podtime.StorageApp.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.denzcoskun.imageslider.models.SlideModel
import com.example.podtime.R
import com.example.podtime.databinding.FragmentSliderBinding


class SliderFragment : Fragment() {
    private var binding: FragmentSliderBinding? = null
    private val imageList = ArrayList<SlideModel>() // Create image list

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSliderBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


// imageList.add(SlideModel("String Url" or R.drawable)
// imageList.add(SlideModel("String Url" or R.drawable, "title") You can add title

        imageList.add(
            SlideModel(
                R.drawable.variant_1,
                "Hand-pickle high quality snacks."
            )
        )
        imageList.add(
            SlideModel(
                "https://bit.ly/2BteuF2",
                "Elephants and tigers may become extinct."
            )
        )
        imageList.add(SlideModel("https://bit.ly/3fLJf72", "And people do that."))
        imageList.add(SlideModel(R.drawable.image, "Title"))
        binding?.imageSlider?.setImageList(imageList)

    }
}