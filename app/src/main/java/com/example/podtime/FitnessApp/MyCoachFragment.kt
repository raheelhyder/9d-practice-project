package com.example.podtime.FitnessApp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.podtime.R
import com.example.podtime.databinding.FragmentMyCoachBinding


class MyCoachFragment : Fragment() {
    private lateinit var binding: FragmentMyCoachBinding
    private lateinit var dataArray: ArrayList<Calender>
    private lateinit var date: Array<String>
    private lateinit var day: Array<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMyCoachBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        day = arrayOf(
            "MON",
            "TUE",
            "WED",
            "THU",
            "FRI",
            "SAT",
            "SUN",
            "MON",
            "TUE",
            "WED",
            "THU",
            "FRI",
            "SAT",
            "SUN",
            "MON",
            "TUE",
            "WED",
            "THU",
            "FRI",
            "SAT",
            "SUN",
            "MON",
            "TUE",
            "WED",
            "THU",
            "FRI",
            "SAT",
            "SUN"
        )

        date = arrayOf(
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28"
        )

        binding.calenderRecycler.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.calenderRecycler.setHasFixedSize(true)
        dataArray = arrayListOf()
        getCalenderData()
/*        binding.calenderRecycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.calenderRecycler.setHasFixedSize(true)
        dataArray = arrayListOf()
        getCalenderData()*/

        binding.coachHeading.setOnClickListener {
            it.findNavController().navigate(R.id.action_myCoachFragment_to_trainingSessionFragment)
        }
    }

    private fun getCalenderData() {
        for (i in date.indices) {
            val calender = Calender(date[i], day[i])
            dataArray.add(calender)
        }
        binding.calenderRecycler.adapter = CalenderAdapter(dataArray)
    }
}