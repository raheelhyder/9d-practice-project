package com.example.podtime.FitnessApp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.podtime.R

class CalenderAdapter(
    private val calender: ArrayList<Calender>,
) :
    RecyclerView.Adapter<CalenderAdapter.CalenderViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CalenderViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.calender_item_layout, parent, false)
        return CalenderViewHolder(view)
    }

    override fun getItemCount(): Int {
        return calender.size
    }

    class CalenderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val date: TextView = itemView.findViewById((R.id.date_tv))
        val day: TextView = itemView.findViewById((R.id.day_tv))
    }

    override fun onBindViewHolder(holder: CalenderViewHolder, position: Int) {
        val calender: Calender = calender[position]
        holder.date.text = calender.date
        holder.day.text = calender.day
    }
}