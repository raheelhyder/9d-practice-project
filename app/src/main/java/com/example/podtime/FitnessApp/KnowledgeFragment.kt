package com.example.podtime.FitnessApp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.podtime.databinding.FragmentKnowledgeBinding


class KnowledgeFragment : Fragment() {
    private lateinit var binding: FragmentKnowledgeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentKnowledgeBinding.inflate(inflater, container, false)
        return binding.root
    }
}