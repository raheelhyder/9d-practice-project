package com.example.podtime.FitnessApp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.example.podtime.R
import com.example.podtime.databinding.FragmentTrainingSessionBinding


class TrainingSessionFragment : Fragment() {
    private lateinit var binding: FragmentTrainingSessionBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTrainingSessionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnCreateChallenge.setOnClickListener {
            it.findNavController().navigate(R.id.action_trainingSessionFragment_to_knowledgeFragment)
        }
    }
}