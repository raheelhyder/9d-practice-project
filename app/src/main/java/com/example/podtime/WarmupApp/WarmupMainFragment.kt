package com.example.podtime.WarmupApp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.podtime.R
import com.example.podtime.databinding.FragmentWarmupMainBinding


class WarmupMainFragment : Fragment() {
    private lateinit var binding: FragmentWarmupMainBinding
    private lateinit var imageList: ArrayList<ImgDataClass>
    private lateinit var imgId: ArrayList<Int>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWarmupMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imgId = arrayListOf(
            R.drawable.training_session,
            R.drawable.young_card,
            R.drawable.photo_gym
        )
        binding.warmupMainRecycler.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.warmupMainRecycler.setHasFixedSize(true)
/*        val startSnapHelper: SnapHelper = StartSnapHelper()
        startSnapHelper.attachToRecyclerView(binding.warmupMainRecycler)*/
        imageList = arrayListOf()
        getData()
        binding.btnStart.setOnClickListener {
            it.findNavController().navigate(R.id.action_warmupMainFragment_to_detailFragment2)
        }
    }

    private fun getData() {
        for (i in imgId.indices) {
            val imgClass = ImgDataClass(imgId[i])
            imageList.add(imgClass)
        }
        binding.warmupMainRecycler.adapter = ItemAdapter(imageList)
    }
}