package com.example.podtime.WarmupApp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.podtime.R
import com.example.podtime.databinding.FragmentDetail2Binding

class DetailFragment : Fragment() {
    private lateinit var binding: FragmentDetail2Binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetail2Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /*Glide.with(this.requireContext()).load(url)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .apply(RequestOptions.bitmapTransform(RoundedCorner(10,0,0,0)))
            .into(binding.summaryCard1Img)*/


        binding.swipe.setOnClickListener {
            findNavController().navigate(R.id.action_detailFragment2_to_techniqueFragment)
        }
        binding.btnCancelTechnique.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.apply {
            uiLogic()

            val corner = resources.getDimension(com.intuit.sdp.R.dimen._6sdp)
            detailCard1Img.setCornersAll(corner)
            detailCard2Img.setCornersAll(corner)
            detailCard3Img.setCornersAll(corner)
        }
    }

    private fun FragmentDetail2Binding.uiLogic() {
        swipe.setCornersTop(25F)
    }
}