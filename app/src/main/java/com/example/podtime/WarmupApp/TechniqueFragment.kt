package com.example.podtime.WarmupApp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.podtime.R
import com.example.podtime.databinding.FragmentTechniqueBinding

class TechniqueFragment : Fragment() {
    private lateinit var binding: FragmentTechniqueBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTechniqueBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnBackTechnique.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}