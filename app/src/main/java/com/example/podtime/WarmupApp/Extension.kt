package com.example.podtime.WarmupApp

import android.graphics.Outline
import android.util.TypedValue
import android.view.View
import android.view.ViewOutlineProvider


fun View.setCornersTop(cornerRadiusDP: Float) {
    val viewOutlineProvider = object : ViewOutlineProvider() {
        override fun getOutline(view: View, outline: Outline) {

            val left = 0
            val top = 0;
            val right = view.width
            val bottom = view.height
            val cornerRadius = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                cornerRadiusDP,
                resources.displayMetrics
            ).toInt()


            //top corners
            outline.setRoundRect(left, top, right, bottom + cornerRadius, cornerRadius.toFloat())

        }
    }
    outlineProvider = viewOutlineProvider
    clipToOutline = true
}

fun View.setCornersAll(cornerRadiusDP: Float) {
    val viewOutlineProvider = object : ViewOutlineProvider() {
        override fun getOutline(view: View, outline: Outline) {

            val left = 0
            val top = 0;
            val right = view.width
            val bottom = view.height
            val cornerRadius = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                cornerRadiusDP,
                resources.displayMetrics
            ).toInt()

            //all corners
            outline.setRoundRect(left, top, right, bottom, cornerRadius.toFloat())
        }
    }
    outlineProvider = viewOutlineProvider
    clipToOutline = true
}