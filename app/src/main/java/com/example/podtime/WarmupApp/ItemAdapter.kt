package com.example.podtime.WarmupApp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.podtime.R


class ItemAdapter(private val list: ArrayList<ImgDataClass>) :
    RecyclerView.Adapter<ItemAdapter.ItemViewholder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ItemViewholder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.warmup_adapter_item, parent, false)
        return ItemViewholder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemViewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: ImageView = itemView.findViewById(R.id.ivWarmupItem)
    }

    override fun onBindViewHolder(holder: ItemViewholder, position: Int) {
        val imageList: ImgDataClass = list[position]
        Glide.with(holder.itemView.context).load(imageList.imgId)
            .centerCrop()
            .into(holder.imageView)
    }
}