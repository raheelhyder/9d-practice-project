package com.example.podtime.PodtimeApp.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.podtime.PodtimeApp.Adapter.DataClasses.SubscriberSub
import com.example.podtime.R


class SubscriberSubAdapter(
    private val subscriberSub: ArrayList<SubscriberSub>,
    //private val classBack: (subTitle: String, price: String) -> Unit
) :
    RecyclerView.Adapter<SubscriberSubAdapter.pViewholder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): pViewholder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.subscriber_sub_item_layout, parent, false)
        return pViewholder(view)
    }

    override fun getItemCount(): Int {
        return subscriberSub.size
    }

    class pViewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvSubscriberSub: TextView = itemView.findViewById((R.id.subscriber_sub_tv))
        val subscribeImageViewSub: ImageView = itemView.findViewById((R.id.subscriber_sub_img))
        val constraintLayout: ConstraintLayout = itemView.findViewById((R.id.subscriber_sub_cons))
    }

    override fun onBindViewHolder(holder: pViewholder, position: Int) {
        val subscribeSub: SubscriberSub = subscriberSub[position]
        holder.tvSubscriberSub.text = subscribeSub.subscriberSubTitle
        Glide.with(holder.itemView.context).load(subscribeSub.subscriberSubImageId)
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .placeholder(R.drawable.ic_group_4226)
            .into(holder.subscribeImageViewSub)

        holder.constraintLayout.setOnClickListener {
            it.findNavController().navigate(R.id.action_detailFragment_to_detailSubFragment)
            //classBack(list[position].watchName, list[position].discountPrice)
        }
        //holder.tvSubscriber.text = subscribeSub.subscriberTitle
        /*holder.constraintLayout.setOnClickListener {
            classBack(list[position].watchName, list[position].discountPrice)
        }*/
    }
}