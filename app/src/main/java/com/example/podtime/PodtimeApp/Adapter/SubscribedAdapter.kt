package com.example.podtime.PodtimeApp.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.podtime.PodtimeApp.Adapter.DataClasses.Subscribe
import com.example.podtime.R

class SubscribedAdapter(
    private val subscribeList: ArrayList<Subscribe>,
    private val classBack: (mainTitle: String, subscriberImage: Int) -> Unit
) :
    RecyclerView.Adapter<SubscribedAdapter.pViewholder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): pViewholder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.subscriber_item_layout, parent, false)
        return pViewholder(view)
    }

    override fun getItemCount(): Int {
        return subscribeList.size
    }

    class pViewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvSubscriber: TextView = itemView.findViewById((R.id.tv_subscriber))
        val subscribeImageView: ImageView = itemView.findViewById((R.id.subscribe_imageView))
        val constraintLayout: ConstraintLayout = itemView.findViewById((R.id.cn_item))
    }

    override fun onBindViewHolder(holder: pViewholder, position: Int) {
        val subscribe: Subscribe = subscribeList[position]
        holder.tvSubscriber.text = subscribe.subscriberTitle
        Glide.with(holder.itemView.context).load(subscribe.subscriberImageId)
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .placeholder(R.drawable.ic_group_4226)
            .into(holder.subscribeImageView)
        holder.constraintLayout.setOnClickListener {
            //it.findNavController().navigate(R.id.action_mainFragment_to_detailFragment)
            classBack(subscribeList[position].subscriberTitle, subscribeList[position].subscriberImageId)
        }
    }
}