package com.example.podtime.PodtimeApp.Adapter.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.podtime.PodtimeApp.Adapter.SubscribedAdapter
import com.example.podtime.PodtimeApp.Adapter.TopicAdapter
import com.example.podtime.PodtimeApp.Adapter.DataClasses.Subscribe
import com.example.podtime.PodtimeApp.Adapter.DataClasses.Topic
import com.example.podtime.R
import com.example.podtime.databinding.FragmentMainBinding


class MainFragment : Fragment() {
    private lateinit var binding: FragmentMainBinding
    private lateinit var myArrayList1: ArrayList<Topic>
    private lateinit var myArrayList2: ArrayList<Subscribe>
    private lateinit var topicName: Array<String>
    private lateinit var topicImage: IntArray
    private lateinit var subscriberText: Array<String>
    private lateinit var subscriberImage: IntArray

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        topicName = arrayOf(
            "Technology",
            "Fire",
            "Nature",
            "Wood",
            "Design",
            "Rain",
            "Flood",
            "Physics",
            "Flower",
            "Drinks"
        )

        topicImage = intArrayOf(
            R.drawable.topic_image_technology,
            R.drawable.topic_image_fire,
            R.drawable.topic_image_nature,
            R.drawable.topic_image_wood,
            R.drawable.topic_image_design,
            R.drawable.topic_image_drinks,
            R.drawable.topic_image_fire,
            R.drawable.topic_image_nature,
            R.drawable.topic_image_wood,
            R.drawable.topic_image_flower
        )

        subscriberText = arrayOf(
            "Explore Human Behavior",
            "Upcoming Technology Update",
            "Flood Disasters",
            "Wood Cutting Conclusions",
            "Design Related Issues",
            "Low Rain",
            "Flood Relief Fund",
            "Physics or Science",
            "Flowers Sprinkles",
            "Drinks Good For Health"
        )

        subscriberImage = intArrayOf(
            R.drawable.topic_image_technology,
            R.drawable.topic_image_fire,
            R.drawable.topic_image_nature,
            R.drawable.topic_image_wood,
            R.drawable.topic_image_design,
            R.drawable.topic_image_drinks,
            R.drawable.topic_image_fire,
            R.drawable.topic_image_nature,
            R.drawable.topic_image_wood,
            R.drawable.topic_image_flower
        )

        binding.rvExploreTopics.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.rvExploreTopics.setHasFixedSize(true)
        myArrayList1 = arrayListOf()
        getTopicData()



        binding.rvSubscribe.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        (binding.rvSubscribe.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(0, 0)
        myArrayList2 = arrayListOf()
        getSubscriberData()
    }

    private fun getTopicData() {
        for (i in topicImage.indices) {
            val topic = Topic(topicName[i], topicImage[i])
            myArrayList1.add(topic)
        }
        binding.rvExploreTopics.adapter = TopicAdapter(myArrayList1)


        /*binding.rvExploreTopics.adapter = TopicAdapter(myArrayList, classBack = { topicName, topicImage ->
            val action = FirstFragmentDirections.actionFirstFragmentToSecondFragment(topicName, topicImage)
            findNavController().navigate(action)
        })*/
    }

    private fun getSubscriberData() {
        for (i in subscriberImage.indices) {
            val subscribe = Subscribe(subscriberText[i], subscriberImage[i])
            myArrayList2.add(subscribe)
        }
        //binding.rvSubscribe.adapter = SubscribedAdapter(myArrayList2)
        binding.rvSubscribe.adapter =
            SubscribedAdapter(myArrayList2, classBack = { subscriberName, subscriberImage ->
                val action = MainFragmentDirections.actionMainFragmentToDetailFragment(
                    subscriberName,
                    subscriberImage
                )
                findNavController().navigate(action)
            })
    }
}