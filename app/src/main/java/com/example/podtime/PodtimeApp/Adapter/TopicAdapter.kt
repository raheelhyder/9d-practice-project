package com.example.podtime.PodtimeApp.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.podtime.PodtimeApp.Adapter.DataClasses.Topic
import com.example.podtime.R


class TopicAdapter(
    private val topic: ArrayList<Topic>,
    /*private val classBack: (topicName: String, topicImage: IntArray) -> Unit*/
) :
    RecyclerView.Adapter<TopicAdapter.AllItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllItemViewHolder {
        val allItem = LayoutInflater.from(parent.context).inflate(R.layout.single_topic, parent, false)
        return AllItemViewHolder(allItem)
    }

    override fun onBindViewHolder(holder: AllItemViewHolder, position: Int) {
        val topic: Topic = topic[position]
        holder.topicName.text = topic.topicName
        Glide.with(holder.itemView.context).load(topic.topicImgId)
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .placeholder(R.drawable.ic_group_4226)
            .into(holder.ivTopic)
    }

    override fun getItemCount(): Int {
        return topic.size
    }

    class AllItemViewHolder(Item: View) : RecyclerView.ViewHolder(Item) {
        val ivTopic: ImageView = itemView.findViewById((R.id.topic_img))
        val topicName: TextView = itemView.findViewById((R.id.topic_tv))
    }
}