package com.example.podtime.PodtimeApp.Adapter.DataClasses

data class PlayingNext(
    var playingNextName: String = "",
    var playingNextTime: String = "",
    var playingNextImgId: Int
)
