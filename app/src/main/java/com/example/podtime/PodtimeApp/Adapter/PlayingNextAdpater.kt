package com.example.podtime.PodtimeApp.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.podtime.PodtimeApp.Adapter.DataClasses.PlayingNext
import com.example.podtime.R

class PlayingNextAdpater(
    private val playingNext: ArrayList<PlayingNext>,
    //private val classBack: (subTitle: String, price: String) -> Unit
) :
    RecyclerView.Adapter<PlayingNextAdpater.pViewholder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): pViewholder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.playing_next_item_layout, parent, false)
        return pViewholder(view)
    }

    override fun getItemCount(): Int {
        return playingNext.size
    }

    class pViewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvPlayingNext: TextView = itemView.findViewById((R.id.tv_playing_next))
        val tvPlayingNextTime: TextView = itemView.findViewById((R.id.tv_playing_next_time))
        val playingNextImageView: ImageView = itemView.findViewById((R.id.playing_next_imageView))
        val constraintLayout: ConstraintLayout = itemView.findViewById((R.id.cn_item_playing_next))
    }

    override fun onBindViewHolder(holder: pViewholder, position: Int) {
        val playingNext: PlayingNext = playingNext[position]
        holder.tvPlayingNext.text = playingNext.playingNextName
        holder.tvPlayingNextTime.text = playingNext.playingNextTime
        Glide.with(holder.itemView.context).load(playingNext.playingNextImgId)
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .placeholder(R.drawable.ic_group_4226)
            .into(holder.playingNextImageView)


    }
}