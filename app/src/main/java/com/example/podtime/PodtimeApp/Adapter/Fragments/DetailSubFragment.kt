package com.example.podtime.PodtimeApp.Adapter.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.podtime.PodtimeApp.Adapter.PlayingNextAdpater
import com.example.podtime.PodtimeApp.Adapter.DataClasses.PlayingNext
import com.example.podtime.R
import com.example.podtime.databinding.FragmentDetailSubBinding
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener


class DetailSubFragment : Fragment() {
    private lateinit var bindingDetailSub: FragmentDetailSubBinding
    private lateinit var playingNextTitle: Array<String>
    private lateinit var playingNextImage: IntArray
    private lateinit var playingNextTime: Array<String>
    private lateinit var detailSubArray: ArrayList<PlayingNext>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bindingDetailSub = FragmentDetailSubBinding.inflate(inflater, container, false)
        return bindingDetailSub.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        playingNextTime = arrayOf(
            "04:30 mins",
            "05:38 mins",
            "12:12 mins",
            "06:43 mins",
            "07:31 mins",
            "08:23 mins",
            "09:54 mins",
            "11:30 mins"
        )

        playingNextImage = intArrayOf(
            R.drawable.topic_image_technology,
            R.drawable.topic_image_fire,
            R.drawable.topic_image_nature,
            R.drawable.topic_image_wood,
            R.drawable.topic_image_design,
            R.drawable.topic_image_drinks,
            R.drawable.topic_image_fire,
            R.drawable.topic_image_nature
        )

        playingNextTitle = arrayOf(
            "1. Know about behaviour",
            "2. Angriness is enemy",
            "3. Love you do",
            "4. Zero to One",
            "5. Never Ending Love",
            "6. Life vs Death",
            "7. Hobson or Jobson",
            "8. Retirement life"
        )


        bindingDetailSub.rvPlayingNext.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        bindingDetailSub.rvPlayingNext.setHasFixedSize(true)
        detailSubArray = arrayListOf()
        getDetailSubArrayData()


       /* val displayMetrics = DisplayMetrics()
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels*/


        val mAppBarLayout: AppBarLayout = bindingDetailSub.appBarLayout
        mAppBarLayout.addOnOffsetChangedListener(object : OnOffsetChangedListener {
            var isShow = false
            var scrollRange = -1
            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
//                if (scrollRange == -1) {
//                    Log.e("if block 1", "onOffsetChanged: $scrollRange")
//                    scrollRange = appBarLayout.totalScrollRange
//                }
                scrollRange = appBarLayout.totalScrollRange
                if (scrollRange + verticalOffset == 0) {
                    Log.e("if block 2", "onOffsetChanged: $scrollRange")
                    isShow = true
                    bindingDetailSub.rippleIconSound1.visibility = View.VISIBLE
                    bindingDetailSub.rippleIconSound2.visibility = View.VISIBLE
                    bindingDetailSub.rippleIconSound3.visibility = View.VISIBLE
                    //showOption(R.id.action_info)
                } else if (isShow) {
                    isShow = false
                    Log.e("if block 3", "onOffsetChanged: $scrollRange")
                    Log.e("if block 3", "onOffsetChanged: $isShow")
                    bindingDetailSub.ivForward301.visibility = View.GONE
                    bindingDetailSub.imageViewPause.visibility = View.GONE
                    bindingDetailSub.ivForward302.visibility = View.GONE
                } else if (scrollRange + verticalOffset <= 463) {
                    Log.e("if block 4", "onOffsetChanged: $scrollRange")
                    isShow = false
                    bindingDetailSub.rippleIconSound1.visibility = View.GONE
                    bindingDetailSub.rippleIconSound2.visibility = View.GONE
                    bindingDetailSub.rippleIconSound3.visibility = View.GONE
                    bindingDetailSub.ivForward301.visibility = View.VISIBLE
                    bindingDetailSub.imageViewPause.visibility = View.VISIBLE
                    bindingDetailSub.ivForward302.visibility = View.VISIBLE
                    //showOption(R.id.action_info)
                }
            }
        })

        bindingDetailSub.backPressImageView2.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun getDetailSubArrayData() {
        for (i in playingNextImage.indices) {
            val playingNext =
                PlayingNext(playingNextTitle[i], playingNextTime[i], playingNextImage[i])
            detailSubArray.add(playingNext)
        }
        bindingDetailSub.rvPlayingNext.adapter = PlayingNextAdpater(detailSubArray)
    }
}