package com.example.podtime.PodtimeApp.Adapter.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.podtime.PodtimeApp.Adapter.SubscriberSubAdapter
import com.example.podtime.PodtimeApp.Adapter.DataClasses.SubscriberSub
import com.example.podtime.R
import com.example.podtime.databinding.FragmentDetailBinding


class DetailFragment : Fragment() {
    private lateinit var bindingDetail : FragmentDetailBinding
    private lateinit var subTitle: Array<String>
    private lateinit var subImage: IntArray
    private val args: DetailFragmentArgs by navArgs()
    private lateinit var subscriberSubArray: ArrayList<SubscriberSub>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bindingDetail = FragmentDetailBinding.inflate(inflater, container, false)
        return bindingDetail.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindingDetail.tvMainHeading.text = args.titleMain
        bindingDetail.imageDetail.setImageResource(args.subscriberImage)
        //bindingDetail.imageDetail.setImageDrawable(context?.getDrawable(activity, R.drawable.your_image));



        subTitle = arrayOf(
            "1. Know about behaviour",
            "2. Angriness is enemy",
            "3. Love you do",
            "4. Zero to One",
            "5. Never Ending Love",
            "6. Life vs Death",
            "7. Hobson or Jobson",
            "8. Retirement life"
        )

        subImage = intArrayOf(
            R.drawable.topic_image_technology,
            R.drawable.topic_image_fire,
            R.drawable.topic_image_nature,
            R.drawable.topic_image_wood,
            R.drawable.topic_image_design,
            R.drawable.topic_image_drinks,
            R.drawable.topic_image_fire,
            R.drawable.topic_image_nature
        )


        bindingDetail.rvSubscribeSub.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        bindingDetail.rvSubscribeSub.setHasFixedSize(true)
        subscriberSubArray = arrayListOf()
        getSubscriberSubArrayData()


        bindingDetail.appBarDetail.gridImageView.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun getSubscriberSubArrayData() {
        for (i in subImage.indices) {
            val subscriberSub = SubscriberSub(subTitle[i], subImage[i])
            subscriberSubArray.add(subscriberSub)
        }
        bindingDetail.rvSubscribeSub.adapter = SubscriberSubAdapter(subscriberSubArray)
    }
}