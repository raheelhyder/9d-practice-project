package com.example.podtime.PodtimeApp.Adapter.DataClasses

data class Topic(
    var topicName: String = "",
    var topicImgId: Int
)