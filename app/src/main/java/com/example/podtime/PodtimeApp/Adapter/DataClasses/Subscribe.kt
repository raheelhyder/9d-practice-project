package com.example.podtime.PodtimeApp.Adapter.DataClasses

data class Subscribe(var subscriberTitle: String, var subscriberImageId: Int)